<?php
require_once(APP . 'Vendor/GDText/src/Box.php');
require_once(APP . 'Vendor/GDText/src/Color.php');
require_once(APP . 'Vendor/GDText/src/TextWrapping.php');
require_once(APP . 'Vendor/GDText/src/HorizontalAlignment.php');
require_once(APP . 'Vendor/GDText/src/VerticalAlignment.php');

use GDText\Box;
use GDText\Color;

App::uses('AppController', 'Controller');
/**
 * PlayedUsers Controller
 *
 * @property PlayedUser $PlayedUser
 * @property PaginatorComponent $Paginator
 */
class PlayedUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
public $components = array('Paginator');

public function beforeFilter(){
	$this->Auth->allow('add','playAnim','getPlayData','preview','test','genVideo','share','downloadVideo','downloadImageMobile','landing');
}


// public $helpers = array(
// 	'Cakegraph.Cakegraph'
// 	);
/**
 * index method
 *
 * @return void
 */
public function index() {
	// $this->theme = 'Ribena';
	// $this->PlayedUser->recursive = 0;
	// $this->set('playedUsers', $this->Paginator->paginate());
	$playedUsers = $this->PlayedUser->find('all',array(
		'conditions'=>array(),
		'order'=>array('PlayedUser.created'=>'DESC')
		));
	$this->set(compact('playedUsers'));
}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function view($id = null) {
	if (!$this->PlayedUser->exists($id)) {
		throw new NotFoundException(__('Invalid played user'));
	}
	$options = array('conditions' => array('PlayedUser.' . $this->PlayedUser->primaryKey => $id));
	$this->set('playedUser', $this->PlayedUser->find('first', $options));
}

/**
 * add method
 *
 * @return void
 */
public function add() {
	$this->theme = 'Ribena';
	if ($this->request->is('post')) {
		if($this->request->data['PlayedUser']['message'] == 'msg1'){
			$this->request->data['PlayedUser']['msg'] = 'msg1';
		}

		if ($this->request->data['PlayedUser']['message'] == 'msg2') {
			$this->request->data['PlayedUser']['msg'] = 'msg2';
		}

		if ($this->request->data['PlayedUser']['message'] == 'msg3') {
			$this->request->data['PlayedUser']['msg'] = 'msg3';
		}

		$this->Session->write('greetings',$this->request->data);

		return $this->redirect(array('action'=>'preview'));
	}

}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function edit($id = null) {
	if (!$this->PlayedUser->exists($id)) {
		throw new NotFoundException(__('Invalid played user'));
	}
	if ($this->request->is(array('post', 'put'))) {
		if ($this->PlayedUser->save($this->request->data)) {
			$this->Flash->success(__('The played user has been saved.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->error(__('The played user could not be saved. Please, try again.'));
		}
	} else {
		$options = array('conditions' => array('PlayedUser.' . $this->PlayedUser->primaryKey => $id));
		$this->request->data = $this->PlayedUser->find('first', $options);
	}
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function delete($id = null) {
	$this->PlayedUser->id = $id;
	if (!$this->PlayedUser->exists()) {
		throw new NotFoundException(__('Invalid played user'));
	}
	$this->request->allowMethod('post', 'delete');
	if ($this->PlayedUser->delete()) {
		$this->Flash->success(__('The played user has been deleted.'));
	} else {
		$this->Flash->error(__('The played user could not be deleted. Please, try again.'));
	}
	return $this->redirect(array('action' => 'index'));
}

public function getPlayData(){
	$this->autoRender = false;
}

public function preview($msg = null,$name = null){ 
	$this->theme = 'Ribena';

	// debug($msg);
	// debug($name);	
	// debug($this->params);

	if(!empty($msg) && !empty($name))
    {

    	$data = array(
    		'PlayedUser'=>array(
    			'msg'=>$msg,
    			'name'=>$name
    			)
    		);
    	$this->Session->write('greetings',$data);
    	// $data['PlayedUser']['msg'] = $msg;
    	// $data['PlayedUser']['name'] = $name;  
        // $imgDir = $this->request->query['imgDir'];
        // $imgName = $this->request->query['imgName'];

        // $imgLink = $imgDir.'/img_'.$imgName.'.png';

        // $getPlayData = $this->PlayedUser->find('first',array(
        //     'conditions'=>array(
        //         'fileLink'=>$imgLink,
        //         )
        //     ));

        // $data = $getPlayData;
    }else{
    	$data = $this->Session->read('greetings');
    }

	
	if ($this->request->is('ajax')) {
		$this->autoRender = false;

    // debug($data);
    // exit;
		$imageVideo = $this->__genImageVideo($data);
		
		//for video generator function
		$this->Session->write('imageLink',$imageVideo['fileLink']);

		$video = $this->genVideo();

		$data['PlayedUser']['fileLink'] = $imageVideo['fileLink'];
		$data['PlayedUser']['vidLink'] = $video['vidLink'];

		$this->PlayedUser->create();
		if ($this->PlayedUser->save($data)) {
			if(is_array($video)){
				return json_encode(
					array(
						'msg'=>'success',
						'vidLink'=>$data['PlayedUser']['vidLink'],
						'imageLink'=>$data['PlayedUser']['fileLink'],
						'name'=>$data['PlayedUser']['name']
						));
			}else{
				debug('error on preview');
				exit;
			}
		}
	}


	$this->set(compact('data'));

}

public function playAnim($imgLink = null){
	$this->theme = 'Ribena';

	$imgDir = $this->request->query['imgDir'];
	$imgName = $this->request->query['imgName'];

	$imgLink = $imgDir.'/img_'.$imgName.'.png';

	$getPlayData = $this->PlayedUser->find('first',array(
		'conditions'=>array(
			'fileLink'=>$imgLink,
			)
		));

	$this->set(compact('getPlayData'));
}

public function genVideo(){
	$this->autoRender = false;

	$date = date('Y-m-d');
	$dateTime = date('YmdHis');

	$imgSource = $this->Session->read('imageLink');
	$imgSource = WWW_ROOT.'files'.DS.$imgSource;
	$vidSource = WWW_ROOT.'base_vid'.DS.'input.mp4';

	$vidFolder = 'files/videos'.DS.$date.DS;

	if (file_exists($vidFolder)) {
		$vidFolder = $vidFolder;
	} else {
		$old = umask(0);

		mkdir('files/videos'.DS.$date.DS, 0775, true);
		umask($old);

		if ($old != umask()) {
			die('An error occurred while changing back the umask');
		}
	}

	$vidFolder = WWW_ROOT.$vidFolder;
	$fileName = 'vid_'.$dateTime.'.mp4';

	$cmd = '/home/admin/bin/ffmpeg -i '.$vidSource.' -i '.$imgSource.' -filter_complex "[1:v]scale=1100:540[out],[0:v][out]overlay=-50:0:enable=\'between(t,0,3)\'" -pix_fmt yuv420p -c:a copy '.$vidFolder.$fileName;
	
	$vidLink = 'files/videos'.DS.$date.DS.$fileName;

	$exec = exec($cmd,$output,$return_var);

	if($return_var == 0){
		return array('vidLink'=>$vidLink);
	}else{
		debug($output);
		debug($return_var);
		return false;
		exit;
	}

}

private function __genImageVideo($grtData){
	$date = date('Y-m-d');

	$imgFolder = 'files'.DS.$date.DS;

	if (file_exists($imgFolder)) {
		$imgFolder = $imgFolder;
	} else {
		$old = umask(0);

		mkdir('files'.DS.$date.DS, 0775, true);
		umask($old);

		if ($old != umask()) {
			die('An error occurred while changing back the umask');
		}
	}

	$imgFolder = WWW_ROOT.$imgFolder;

	if($grtData['PlayedUser']['msg'] == 'msg1'){
		$bgSource = WWW_ROOT.'img/base_img/video_bg_1.png';
	}

	if($grtData['PlayedUser']['msg'] == 'msg2'){
		$bgSource = WWW_ROOT.'img/base_img/video_bg_2.png';
	}

	if($grtData['PlayedUser']['msg'] == 'msg3'){
		$bgSource = WWW_ROOT.'img/base_img/video_bg_3.png';
	}
	

	$image = @imagecreatefrompng($bgSource);

	$dateTime = date('YmdHis');
	$fileName = 'img_'.$dateTime.'.png';
	$output = $imgFolder.$fileName;

	$white = imagecolorallocate($image, 255, 255, 255);
	$black = imagecolorallocate($image,0,0,0);
	$grey = imagecolorallocate($image, 128, 128, 128);



	$imageWidth = imagesx($image);  
	$imageHeight = imagesy($image);

	$userName = $grtData['PlayedUser']['name'];

	if(preg_match("/\p{Han}+/u", $userName)){
		$fontFile = WWW_ROOT.'fonts/wts11.ttf';
		$fontSize = 60;
		$rotation = 0;
	}else{
		$fontFile = WWW_ROOT.'fonts/LemonMilk.ttf';
		$fontSize = 60;
		$rotation = 0;
	}

	$box = new GDText\Box($image);
	$box->setFontFace($fontFile);
	$box->setFontSize($fontSize);
	$box->setFontColor(new Color(255, 255, 255));
	$box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
	$box->setBox(0, 300, $imageWidth, $imageHeight);
	$box->setTextAlign('center', 'center');
	$box->draw($userName);
	imagejpeg($image,$output);

	$fileLink = $date.DS.$fileName;

	return array('fileLink'=>$fileLink);
}

public function landing(){
	$this->theme = 'Ribena';
}

}
