<div class="row pre-panel">
	<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-4">
		<div class="panel panel-default fc-panel">
			<div class="panel-body">
				<?php echo $this->Form->create('PlayedUser',array('id'=>'confirm')); ?>

				<?php //debug($data);?>
				<?php if($data['PlayedUser']['msg'] == 'msg1'):?>
					<?php echo $this->Html->image('ribena/CNY_Greeting-01.png',array('fullBase'=>true,'class'=>'img-responsive'));?>
				<?php endif;?>
				<?php if($data['PlayedUser']['msg'] == 'msg2'):?>
					<?php echo $this->Html->image('ribena/CNY_Greeting-02.png',array('fullBase'=>true,'class'=>'img-responsive'));?>
				<?php endif;?>
				<?php if($data['PlayedUser']['msg'] == 'msg3'):?>
					<?php echo $this->Html->image('ribena/CNY_Greeting-03.png',array('fullBase'=>true,'class'=>'img-responsive'));?>
				<?php endif;?>
				<h3 class="text-center">
					FROM
				</h3>
				<h2 class="text-center">
					<?php echo $data['PlayedUser']['name']?>
				</h2>

				<a href="<?php echo $this->webroot;?>msg" class="pull-left"><?php echo $this->Html->image('back_btn.png',array('fullBase'=>true,'width'=>100));?></a>

				<input type="image" name="submit" value="Confirm" src="https://ribenabeme.com/greeting/theme/Ribena/img/confirm_btn.png" border="0" width="100" class="pull-right"/>
				
				<?php echo $this->Form->end();?>
			</div>
		</div>
	</div>
</div>
<div class="modal"></div>
<script>
	$(document).ready(function() {

		$body = $("body");

		$(document).on({
			ajaxStart: function() { $body.addClass("loading"); }, 
		});

		var domain = 'https://ribenabeme.com';
		var projD = '/greeting';
		var url = '/playedUsers/preview'

		$('#confirm').submit(function(event) {
			event.preventDefault();
			$.ajax({
				url: domain+projD+url,
				type: 'POST',
			})
			.done(function(data) {
				var data = JSON.parse(data);
				if(data['msg'] == 'success'){
					var imgName= data['imageLink'].split('_')[1];
					imgName = imgName.substring(0, imgName.indexOf('.'));
					var imgDir = data['imageLink'].substr(0, 10);
					var imgLink = 'http://ribenabeme.com/cny/files/'+imgDir+'/img_'+imgName+'.png';

					var name = encodeURIComponent(data['name']);

					window.location = domain+projD+'/v/?imgDir='+imgDir+'&imgName='+imgName+'&name='+name;
				}	
			})
			.fail(function(data) {
				console.log("error");
				console.log("data");
			})
		});
	});
</script>
